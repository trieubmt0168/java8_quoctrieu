package com.dd.Trieu.Lambda;

import java.math.BigDecimal;
import java.util.*;


public class TestSorting     {


     private static List<Developer> getDevelopers() {
        List <Developer> result = new ArrayList<Developer>();
        result.add(new Developer("Trieu", new BigDecimal(600000),20));
         result.add(new Developer("Duy", new BigDecimal(700000),22));
         result.add(new Developer("Long", new BigDecimal(800000),23));
         result.add(new Developer("Quan", new BigDecimal(900000),24));

         return result;
     }
    public static void main(String[] args) {
        List<Developer> listdeveloper = getDevelopers();
        System.out.println("before Sort");

        for(Developer x : listdeveloper) {
            System.out.println(x);
        }
        //sort  by age
        Collections.sort(listdeveloper, new Comparator<Developer>() {
            @Override
            public int compare(Developer o1, Developer o2) {
                return o1.getAge() - o2.getAge();
            }
        });
        System.out.println("After Sort");
        for (Developer x : listdeveloper){
            System.out.println(x);
        }
    }
}
