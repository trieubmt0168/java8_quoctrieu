package com.dd.Trieu.Lambda;

import com.dd.Trieu.Lambda.Developer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestSorting {
    private static List<com.dd.Trieu.Lambda.Developer> getDevelopers() {
        List<Developer> result = new ArrayList<com.dd.Trieu.Lambda.Developer>();
        result.add(new Developer("Trieu", new BigDecimal(600000), 22));
        result.add(new Developer("Duy", new BigDecimal(800000), 21));
        result.add(new Developer("Long", new BigDecimal(700000), 24));
        result.add(new Developer("Quan", new BigDecimal(500000), 23));

        return result;
    }

    public static void main(String[] args) {
        List<Developer> listdeveloper = getDevelopers();
        System.out.println("before Sort");

        for (Developer x : listdeveloper) {
            System.out.println(x);
        }

        //lambda tuoi
        System.out.println();
        System.out.println();
        System.out.println("Tuổi");
        listdeveloper.sort((Developer o1, Developer o2)->o1.getAge()-o2.getAge());
        listdeveloper.forEach((developer)->System.out.println(developer));

        //lambda Luong
        System.out.println();
        System.out.println();
        System.out.println("Luong");
        listdeveloper.sort((Developer o1, Developer o2) -> o1.getSalary().compareTo(o2.getSalary()));
        listdeveloper.forEach((developer)->System.out.println(developer));
            //Lambda ten
        System.out.println();
        System.out.println();
        System.out.println("Tên");
        listdeveloper.sort((Developer o1, Developer o2) -> o1.getName().compareTo(o2.getName()));
        listdeveloper.forEach((developer)->System.out.println(developer));

        //sắp xếp đảo ngược
        System.out.println();
        System.out.println();
        System.out.println("Đảo ngược tiền lương");
    Comparator<Developer> salaComparator = (o1,o2) ->o1.getSalary().compareTo(o2.getSalary());
    listdeveloper.sort(salaComparator.reversed());
    listdeveloper.forEach((developer -> System.out.println(developer)));
        System.out.println("After Sort");
        for (Developer x : listdeveloper) {
            System.out.println(x);
        }
    }
}


