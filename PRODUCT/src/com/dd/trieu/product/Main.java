package com.dd.trieu.product;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
        LocalDate today = LocalDate.now();
        LocalDate nextweek = LocalDate.ofEpochDay(7);
        LocalDate nextmonth = LocalDate.ofEpochDay(30);
        List<Product> productList = new ArrayList<>();

        productList.add(new Product(1, "Long", 1, today, 0, false));
        productList.add(new Product(2, "Thành", 2, today, 1, true));
        productList.add(new Product(3, "Thái", 2, nextweek, 6, true));
        productList.add(new Product(4, "Bưởi", 3, nextweek, 4, true));
        productList.add(new Product(5, "Lò", 3, nextmonth, 0, true));
        productList.add(new Product(6, "Mận", 2, nextmonth, 0, false));
        productList.add(new Product(7, "táo", 2, today, 0, false));
        productList.add(new Product(8, "Nho", 1, today, 0, false));
        productList.add(new Product(9, "Cam", 1, nextmonth, 0, true));
        productList.add(new Product(10, "Mướp", 1, nextmonth, 0, false));


        //Lấy id theo tên kiểu Stream
        System.out.println();
        System.out.println("----lấy tên kiểu Stream----");
        String name = productList.stream()
                .filter(x -> 1 == x.getId())
                .map(Product::getName)
                .findAny()
                .orElse("");
        System.out.println("name  :" + name);


        /// Lấy id theo tên kiểu thường
        System.out.println();
        System.out.println("--Lấy id theo tên kiểu thường----");
        Product result = filterProductByid(productList, 6);
        System.out.println(result.getName());


        ////bài 12
        System.out.println();
        System.out.println("Trả về arr dùng bằng Stream");
        List<Product> products = productList.stream().filter(x -> x.isDelete() == false && x.getQulity() > 0).collect(Collectors.toList());
        System.out.println(products);

        ///
        System.out.println();
        System.out.println("Trả về arr không dùng Stream");
        List<Product> products1 = filterQuilityIsDelete(productList, false);
        System.out.println(products1);

        ////Bài 13
        System.out.println();
        System.out.println("Trả về arr có saleDaTe dùng bằng Stream");
        List<Product> products2 = productList.stream().filter(x -> x.isDelete() == false && x.getSaleDate().isAfter(today)).collect(Collectors.toList());
        System.out.println(products2);

        //không dùng Stream
        System.out.println();
        System.out.println("Trả về arr không dùng Stream bài 13");
        List<Product> products3 = filterProductBySaleDate(productList, false);
        System.out.println(products3);

        ///////////////////////////////////////////////////////////
        //Bài 14
//        System.out.println("trả về tổng product chưa bị xóa bài 14");
//        List<Product> products4 = productList.stream().filter(x -> x.isDelete() == false ).collect(Collectors.toList());
//        System.out.println(products4 + sum);

        ///không dùng stream bài 14
//        System.out.println();
//        System.out.println("Trả về arr không dùng Stream bài 13");
//        List<Product> products5 = totalProduct(productList, false);
//        System.out.println(products1);



        ///////////////
        //Bài 15
        System.out.println();
        System.out.println("Trả về arr có true nếu product thuộc category dùng bằng Stream");
        List<Product> products5 = productList.stream().filter(x -> x.isDelete() == true && x.getCategoryid() == x.getId()  ).collect(Collectors.toList());
        System.out.println(products5);

        //Bài 15
//        System.out.println();
//        System.out.println("Trả về arr có true nếu product thuộc category không dùng  Stream");
//        List<Product> products6 = totalProduct(productList, false);
//        System.out.println(products1);
    }
//Bài 14
//    private static List<Product> totalProduct(List<Product> productList, boolean isDelete) {
//        List<Product> products = new ArrayList<Product>();
//
//        for (boolean pr : productList) {
//
//
//        }
//        return products;
//    }


    private static List<Product> filterProductBySaleDate(List<Product> productList, boolean isDelete) {
        List<Product> products = new ArrayList<Product>();
        for (Product x : productList) {
            if (x.getSaleDate().isAfter(LocalDate.now()))
                if (isDelete == false) {
                    products.add(x);
                }
        }
        return products;
    }

    private static List<Product> filterQuilityIsDelete(List<Product> productList, boolean isDelete) {
        List<Product> products = new ArrayList<Product>();
        for (Product x : productList) {
            if (x.getQulity() > 0 && isDelete == x.isDelete()) {
                products.add(x);
            }
        }
        return products;
    }

    private static Product filterProductByid(List<Product> productEntityList, Integer id) {
        Product product = null;
        for (Product temp : productEntityList) {
            if (id == temp.getId()) {
                product = temp;
            }
        }
        return product;
    }


}
