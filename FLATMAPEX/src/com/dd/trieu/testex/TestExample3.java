package com.dd.trieu.testex;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

// Stream + Primitive + flatMapToInt
public class TestExample3 {
    public static void main(String[] args) {
        int[] intArray = {1, 2, 3, 4, 5, 6};

        //Stream<int[]>
        Stream<int[]> streamArrays = Stream.of(intArray);

        //2. Stream<int[]> -> flatMap -> IntStream
        IntStream intStream = streamArrays.flatMapToInt(x -> Arrays.stream(x));

        intStream.forEach(x -> System.out.println(x));
    }
}
