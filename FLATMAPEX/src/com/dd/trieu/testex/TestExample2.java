package com.dd.trieu.testex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

// sử dụng latMAp và set
public class TestExample2 {
    public static void main(String[] args) {
        Student obj1 = new Student();
        obj1.setName("Thái");
        obj1.addBook("sách đẹp");
        obj1.addBook("Sách hay");


        Student obj2 = new Student();
        obj2.setName("Tuân");
        obj2.addBook("Sách học");
        obj2.addBook("sách chơi");


        List<Student> list = new ArrayList<>();
        list.add(obj1);
        list.add(obj2);

        List<String> collect = list.stream()
                .map(x -> x.getBook())
                .flatMap(x -> x.stream())
                .distinct()
                .collect(Collectors.toList());

        collect.forEach(x -> System.out.println(x));

//        System.out.println("Tên người");
//        List<String> collect1 =  list.stream()
//                .map(x ->x.getName().)
//                .flatMap(x ->x.stream)
    }
}
