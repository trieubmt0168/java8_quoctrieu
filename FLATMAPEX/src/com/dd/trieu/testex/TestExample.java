package com.dd.trieu.testex;

import java.util.Arrays;
import java.util.stream.Stream;

public class TestExample {
    public static void main(String[] args) {

        String[][] data = new String[][]{{"a", "b"}, {"c", "d"}, {"e", "f"}};

        //Stream<String>
        Stream<String[]> temp = Arrays.stream(data);

        //Stream<String> , good
        //chuyển đổi Stream ban đầu thành những Stream mới.
        Stream<String> StringStream = temp.flatMap(x -> Arrays.stream(x));

        Stream<String> stream = StringStream.filter(x -> "a".equals(x.toString()));

        stream.forEach(System.out::println);
        //các intermediate operation có 5 phương thức: filter(), map(), limit(), sorted(), distinct()
    /*    //Lọc 1 chuỗi và trả về 1 chuỗi.
        Stream<String[]> stream = temp.filter(x -> "a".equals(x.toString()));
        stream.forEach(System.out::println);*/
    }
}
