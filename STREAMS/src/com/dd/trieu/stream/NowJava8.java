package com.dd.trieu.stream;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class NowJava8 {
    public static void main(String[] args) {
        List<Person> personList = Arrays.asList(
                new Person("Phong", 22),
                new Person("Triệu", 21),
                new Person("Duy", 20)
        );
        // Tìm kiếm thất phần tử
        Person person = personList.stream() //dạng stream
                .filter(x -> "Phong".equals(x.getName())) //Chỉ muốn 1 phần tử nào đó
                .findAny()// tìm thấy bất kì
                .orElse(null);// không thấy trả về null.

        System.out.println(person);

        //tìm kiếm theo tên và tuổi nhưng tuổi không chính xác
        Person person2 = personList.stream() //dạng stream
                .filter((x) -> "Phong".equals(x.getName()) && 30 == x.getAge()) //Chỉ muốn 1 phần tử nào đó
                .findAny()// tìm thấy bất kì
                .orElse(null);// không thấy trả về null.

        System.out.println(person2);


        //Không tìm thấy phần tử
        Person person1 = personList.stream()
                .filter(x -> "aaa".equals(x.getName()))
                .findAny()
                .orElse(null);
        System.out.println(person1);
        // Đối với nhiều điều kiện
        Person person3 = personList.stream()
                .filter(
                        x -> {
                            if ("Triệu".equals(x.getName()) && 20 == x.getAge()) {
                                return true;
                            }
                            return false;
                            /* nếu đặt return đầu bằng true,
                             return tiếp theo bằng true dữ liêu nhập vào nếu sai hết
                             thì nó sẽ lấy giá trị đầu tiên của dữ liệu*/

                            /*Nếu đặt return bằng true trong if  và rutuen tiếp theo ở ngoài if
                            thì nó sẽ trả về null, không tìm thấy phần tử nào*/
                        }).findAny()
                .orElse(null);
        System.out.println("persion3 :" + person3);
        /// Xuất tên


        System.out.println("----- Xuất tên Muốn gọi ------");
        String name = personList.stream()
                .filter(x -> "22".equals(x.getAge()))
                .map(Person::getName)
                .findAny()
                .orElse("");
        System.out.println("name  :" + name);


        System.out.println("----- Xuất all tên ------");
        List<String> collect = personList.stream()
                .map(Person::getName)///map tất cả các tên ra ngoài màn hình
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }
}
