package com.dd.trieu.stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class BeforeJava8 {
    public static void main(String[] args) {
        List<String> lines = Arrays.asList("Long", "Tuấn", "Tú");
        /*Tìm kiếm và lọc, nếu tên được gọi ra mà khác vói các tên còn
       lại thì tên gọi ra sẽ đóng, các tên khác sẽ hiển thị lên màn hình */
        List<String> result = lines.stream().filter(line -> !"Long".equals(line)).filter(line -> !"Tuấn".equals(line)).collect(Collectors.toList());
        result.forEach(System.out::println);
    }
}