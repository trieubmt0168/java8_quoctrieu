package com.dd.trieu.product1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Product {
    private int id;
    private String name;
    private int categoryid;
    private Date saleDate;
    private int qulity;
    private boolean isDelete;

    public Product(int id, String name, int categoryid, Date saleDate, int qulity, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.categoryid = categoryid;
        this.saleDate = saleDate;
        this.qulity = qulity;
        this.isDelete = isDelete;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(int categoryid) {
        this.categoryid = categoryid;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public int getQulity() {
        return qulity;
    }

    public void setQulity(int qulity) {
        this.qulity = qulity;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryid=" + categoryid +
                ", saleDate=" + saleDate +
                ", qulity=" + qulity +
                ", isDelete=" + isDelete +
                '}';
    }

    public static List<Product> getPRoduct() throws ParseException {
        List<Product> productList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");

        productList.add(new Product(1, "Long", 1, dateFormat.parse("09/05/2019"), 1, false));
        productList.add(new Product(2, "Thành", 2, dateFormat.parse("9/05/2019"), 1, true));
        productList.add(new Product(3, "Thái", 2, dateFormat.parse("9/05/2019"), 6, true));
        productList.add(new Product(4, "Bưởi", 3, dateFormat.parse("9/05/2019"), 4, true));
        productList.add(new Product(5, "Lò", 3, dateFormat.parse("9/05/2019"), 0, true));
        productList.add(new Product(6, "Mận", 2, dateFormat.parse("9/05/2019"), 0, false));
        productList.add(new Product(7, "táo", 2, dateFormat.parse("9/05/2019"), 0, false));
        productList.add(new Product(8, "Nho", 1, dateFormat.parse("9/05/2019"), 0, false));
        productList.add(new Product(9, "Cam", 1, dateFormat.parse("9/05/2019"), 0, true));
        productList.add(new Product(10, "Mướp", 1, dateFormat.parse("9/05/2019"), 1, false));

        return productList;
    }
}
