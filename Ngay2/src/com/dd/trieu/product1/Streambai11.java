package com.dd.trieu.product1;

import java.util.List;

public class Streambai11 {
    public static Product filterProductByid(List<Product> productEntityList, Integer id) {

        Product product = productEntityList.stream().filter(x -> id == x.getId()).findAny().orElse(null);

        return product;

    }
}
